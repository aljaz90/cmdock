#ifndef _RBTHEAP_H_
#define _RBTHEAP_H_

#include <queue>
#include <vector>
#include "Rbt.h"

namespace RbtHeap {
    struct RBTDLL_EXPORT Ligand {
        double score;
        std::vector<std::string> ligand;

        Ligand(double dScore, std::vector<std::string> vLigand);
    };

    struct SizeComp {
        bool operator()(const Ligand& a, const Ligand& b){
            return a.score < b.score;
        }
    };

    struct RBTDLL_EXPORT Heap {
        unsigned long limit;
        std::priority_queue<
            Ligand, std::vector<Ligand>, SizeComp
        > heap;
        
        Heap(){};
        Heap(unsigned long uSize);

        void setSize(unsigned long uSize);
        size_t size();
        Ligand top();
        void pop();
        bool empty();
        void push(Ligand newLigand);
        void clear();
    };
}

#endif