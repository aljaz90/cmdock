#!/usr/bin/python

import sys
import difflib

#############################################################################
# ARGUMENTS CHECK
#############################################################################
if len(sys.argv) != 3:
    print('Wrong number of arguments')
    print('Usage: compare.py file1 file2')
    sys.exit(1)

file1 = sys.argv[1]
file2 = sys.argv[2]

#############################################################################
# COMPARE TWO FILES
#############################################################################
## Using unified diff format from the difflib library
with open(file1,'r') as f:
    file1_content = f.readlines()

with open(file2,'r') as f:
    file2_content = f.readlines()

changes = list(difflib.unified_diff(file1_content, file2_content, fromfile=file1, tofile=file2))
if changes:
    print("Files are not identical. Showing unified diff:")
    sys.stdout.writelines(changes)
    sys.exit(1)

sys.exit(0)